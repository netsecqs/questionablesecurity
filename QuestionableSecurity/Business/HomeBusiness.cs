﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace QuestionableSecurity.Business
{
    /// <summary>
    /// A class for handling business logic for the Home controller.
    /// </summary>
    public class HomeBusiness
    {

        public HomeBusiness()
        {

        }

        public HashSet<string> retrieveDictionary(Cache cache, string appDataPath, string dataSet)
        {
            switch(dataSet)
            {
                case "PetNames":
                    return loadDictionary(cache, appDataPath, dataSet, "petnames.txt");
                case "FemaleNames":
                    return loadDictionary(cache, appDataPath, dataSet, "femalenames.txt");
                case "MaleNames":
                    return loadDictionary(cache, appDataPath, dataSet, "malenames.txt");
                case "Cities":
                    return loadDictionary(cache, appDataPath, dataSet, "HighPopulationCities.txt");
                case "Mascots":
                    return loadDictionary(cache, appDataPath, dataSet, "Mascots.txt");
                case "Lastnames":
                    return loadDictionary(cache, appDataPath, dataSet, "Lastnames.txt");
                case "MixedNames":
                    return loadDictionary(cache, appDataPath, dataSet, "mixednames.txt");
                default:
                    return null;
            }
        }

        /// <summary>
        /// Obtains the desired dataset's dictionary from the cache. All names are lowercase.
        /// </summary>
        private HashSet<string> loadDictionary(Cache cache, string appDataPath, string dataSetToLoad, string textFile)
        {
            HashSet<string> names = (HashSet<string>)cache.Get(dataSetToLoad);

            //Already cached? Return it
            if (names != null)
            {
                return names;
            }

            //Read in from the file
            using (StreamReader reader = new StreamReader(File.Open(appDataPath + textFile, FileMode.Open)))
            {
                string[] strings = reader.ReadToEnd().ToLower().Split("\r\n".ToCharArray());
                names = new HashSet<string>();

                foreach (string s in strings)
                {
                    names.Add(s.Trim());
                }
            }

            //Store in the cache
            cache[dataSetToLoad] = names;


            return names;
        }


        /// <summary>
        /// Returns a list of security questions from the cache.
        /// The security questions are guaranteed to be in the same
        /// positions in the list every time.
        /// </summary>
        public List<string> GetSecurityQuestions(Cache cache)
        {
            List<string> questions = (List<string>)cache.Get("SecurityQuestions");

            if (questions != null)
            {
                return questions;
            }


            questions = new List<string>(
                new string[] { 
                    "What is your father's middle name?",
                    "In what city were you married?",
                    "In what city was your high school?",
                    "What was the first name of the maid of honor at your wedding?",
                    "What is your maternal grandmother's first name?",
                    "What is the first name of your oldest nephew?",
                    "What is your best friend's first name?",
                    "What is your maternal grandfather's first name?",
                    "What was your high school mascot?",
                    "What was the first name of the best man at your wedding?",
                    "What was the name of your first pet?",
                    "What is your paternal grandmother's first name?",
                    "What was the first name of your first manager?",
                    "In what city was your father born?",
                    "In what city is your vacation home?",
                    "What was the name of your first girlfriend/boyfriend?",
                    "What is the first name of your oldest niece?",
                    "What was the nickname of your grandfather?",
                    "What street did your best friend in high school live on?",
                    "What was your favorite restaurant in college?",
                    "What is your paternal grandfather's first name?",
                    "What was the name of the town your grandmother lived in?",
                    "What was the last name of your favorite teacher in the final year of high school?",
                    "In what city was your mother born?",
                    "Where did you meet your spouse for the first time?",
                    "What is your mother's middle name?",
                    "In what city were you born?",
                    "What was the name of your junior high school?"
                });

            cache["SecurityQuestions"] = questions;

            return questions;

        }

        /// <summary>
        /// Runs a dictionary attack against the given question and answer.
        /// </summary>
        public Tuple<Tuple<string, double, int>, List<string>> DictionaryAttack(int QuestionId, string Answer, Cache cache, string appDataPath)
        {
            Tuple<string, double, int> dictResult;
            List<string> distResult = new List<string>();
            HashSet<string> dict = new HashSet<string>();

            switch (QuestionId)
            {
                case 0:     //"What is your father's middle name?"
                    dict = new HashSet<string>(retrieveDictionary(cache, appDataPath, "MaleNames")
                        .Concat(retrieveDictionary(cache, appDataPath, "MixedNames")));
                    break;
                case 1:     //"In what city were you married?"
                    dict = retrieveDictionary(cache, appDataPath, "Cities");
                    break;
                case 2:     //"In what city was your high school?"
                    dict = retrieveDictionary(cache, appDataPath, "Cities");
                    break;
                case 3:     //"What was the first name of the maid of honor at your wedding?"
                    dict = new HashSet<string>(retrieveDictionary(cache, appDataPath, "FemaleNames")
                        .Concat(retrieveDictionary(cache, appDataPath, "MixedNames")));
                    break;
                case 4:     //"What is your maternal grandmother's first name?"
                    dict = new HashSet<string>(retrieveDictionary(cache, appDataPath, "FemaleNames")
                        .Concat(retrieveDictionary(cache, appDataPath, "MixedNames")));
                    break;
                case 5:     //"What is the first name of your oldest nephew?"
                    dict = new HashSet<string>(retrieveDictionary(cache, appDataPath, "MaleNames")
                        .Concat(retrieveDictionary(cache, appDataPath, "MixedNames")));
                    break;
                case 6:     //"What is your best friend's first name?"
                    dict = new HashSet<string>(retrieveDictionary(cache, appDataPath, "MaleNames")
                        .Concat(retrieveDictionary(cache, appDataPath, "FemaleNames")));
                    break;
                case 7:     //"What is your maternal grandfather's first name?"
                    dict = new HashSet<string>(retrieveDictionary(cache, appDataPath, "MaleNames")
                        .Concat(retrieveDictionary(cache, appDataPath, "MixedNames")));
                    break;
                case 8:     //"What was your high school mascot?"
                    dict = retrieveDictionary(cache, appDataPath, "Mascots");
                    break;
                case 9:     //"What was the first name of the best man at your wedding?"
                    dict = new HashSet<string>(retrieveDictionary(cache, appDataPath, "MaleNames")
                        .Concat(retrieveDictionary(cache, appDataPath, "MixedNames")));
                    break;
                case 10:    //"What was the name of your first pet?"
                    dict = new HashSet<string>(retrieveDictionary(cache, appDataPath, "PetNames")
                        .Concat(retrieveDictionary(cache, appDataPath, "MaleNames"))
                        .Concat(retrieveDictionary(cache, appDataPath, "FemaleNames"))
                        .Concat(retrieveDictionary(cache, appDataPath, "MixedNames")));
                    break;
                case 11:    //"What is your paternal grandmother's first name?"
                    dict = retrieveDictionary(cache, appDataPath, "FemaleNames");
                    break;
                case 12:    //"What was the first name of your first manager?"
                    dict = new HashSet<string>(retrieveDictionary(cache, appDataPath, "MaleNames")
                        .Concat(retrieveDictionary(cache, appDataPath, "FemaleNames"))
                        .Concat(retrieveDictionary(cache, appDataPath, "MixedNames")));
                    break;
                case 13:    //"In what city was your father born?"
                    dict = retrieveDictionary(cache, appDataPath, "Cities");
                    break;
                case 14:    //"In what city is your vacation home?"
                    dict = retrieveDictionary(cache, appDataPath, "Cities");
                    break;
                case 15:    //"What was the name of your first girlfriend/boyfriend?"
                    dict = new HashSet<string>(retrieveDictionary(cache, appDataPath, "MaleNames")
                        .Concat(retrieveDictionary(cache, appDataPath, "FemaleNames"))
                        .Concat(retrieveDictionary(cache, appDataPath, "MixedNames")));
                    break;
                case 16:    //"What is the first name of your oldest niece?"
                    dict = new HashSet<string>(retrieveDictionary(cache, appDataPath, "FemaleNames")
                        .Concat(retrieveDictionary(cache, appDataPath, "MixedNames")));
                    break;
                case 17:    //"What was the nickname of your grandfather?"
                    dict = new HashSet<string>(retrieveDictionary(cache, appDataPath, "MaleNames")
                        .Concat(retrieveDictionary(cache, appDataPath, "MixedNames")));
                    break;
                case 18:    //"What street did your best friend in high school live on?"

                    //no dict applicable
                    break;
                case 19:    //"What was your favorite restaurant in college?"

                    //no dict applicable
                    break;
                case 20:    //"What is your paternal grandfather's first name?"
                    dict = new HashSet<string>(retrieveDictionary(cache, appDataPath, "MaleNames")
                        .Concat(retrieveDictionary(cache, appDataPath, "MixedNames")));
                    break;
                case 21:    //"What was the name of the town your grandmother lived in?"
                    dict = retrieveDictionary(cache, appDataPath, "Cities");
                    break;
                case 22:    //"What was the last name of your favorite teacher in the final year of high school?"
                    dict = retrieveDictionary(cache, appDataPath, "Lastnames");
                    break;
                case 23:    //"In what city was your mother born?"
                    dict = retrieveDictionary(cache, appDataPath, "Cities");
                    break;
                case 24:    //"Where did you meet your spouse for the first time?"
                    dict = retrieveDictionary(cache, appDataPath, "Cities");
                    break;
                case 25:    //""What is your mother's middle name?"
                    dict = new HashSet<string>(retrieveDictionary(cache, appDataPath, "FemaleNames")
                        .Concat(retrieveDictionary(cache, appDataPath, "MixedNames")));
                    break;
                case 26:    //"In what city were you born?"
                    dict = retrieveDictionary(cache, appDataPath, "Cities");
                    break;
                case 27:    //"What was the name of your junior high school?"

                    //no dict applicable
                    break;
                default:
                    break;
            }

            dictResult = checkDictionary(dict, Answer);
            distResult = checkDictionaryEditDistance(dict, Answer);

            return new Tuple<Tuple<string, double, int>, List<string>>(dictResult, distResult);
        }

        /// <summary>
        /// Creates a Select List for the security questions.
        /// </summary>
        public SelectList GetQuestionSelectList(List<string> questions)
        {
            List<KeyValuePair<int, string>> map = new List<KeyValuePair<int, string>>(questions.Count);
            
            for (int i = 0; i < questions.Count; i++)
            {
                map.Add(new KeyValuePair<int, string>(i, questions[i]));
            }

            return new SelectList(map, "Key", "Value");
        }

        /// <summary>
        /// Checks the dictionary for the answer string.
        /// </summary>
        public Tuple<string, double, int> checkDictionary(HashSet<string> dictionary, string answer)
        {
            
            //start the timer
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();


            //check the string against the dictionary
            answer = answer.ToLower();

            if (dictionary.Contains(answer))
            {
                stopwatch.Stop();
                return new Tuple<string, double, int>(answer, stopwatch.ElapsedMilliseconds, 0);
            }

            //foreach(string s in dictionary)
            //{
            //    //check if the string is the same ignoring whitespace and 
            //    if(String.Equals(answer, s, StringComparison.OrdinalIgnoreCase))
            //    {
            //        //stop the timer
            //        stopwatch.Stop();

            //        //send the string that got caught in our system
            //        return new Tuple<string, double, int>(s, stopwatch.ElapsedMilliseconds, 0);
            //    }
            //}

            //stop the timer
            stopwatch.Stop();


            return new Tuple<string, double, int>(null, stopwatch.ElapsedMilliseconds, 0);
        }

        /// <summary>
        /// Obtains a list of all strings in the dictionary with an edit distance under 20% of the answer's length.
        /// This accounts for "clever" changes like a '0' in place of an 'o'.
        /// </summary>
        public List<string> checkDictionaryEditDistance(IEnumerable<string> dictionary, string answer)
        {

            answer = answer.ToLower();
            //create a threshold for the edit distance of the string
            int threshold = (int)(((double)answer.Length) * .2);
            if (threshold < 1)
            {
                threshold = 1;
            }
            List<string> closeAnswers = new List<string>(); 

            foreach(string s in dictionary)
            {
                int editDist = ComputeEditDistance(s, answer, threshold);

                if(editDist <= threshold && answer != s)
                {
                    closeAnswers.Add(s);
                }
            }


            return closeAnswers;
        }

        /**
         * Computes the edit of two strings. 
         * This method was taken from Levenshtein and is not orginal work by us.
         * 
         */
        private int ComputeEditDistance(string s, string t, int threshold)
        {
            int n = s.Length;
            int m = t.Length;
            int[,] d = new int[n + 1, m + 1];

            // Step 1
            if (n == 0)
            {
                return m;
            }

            if (m == 0)
            {
                return n;
            }

            // Step 2
            for (int i = 0; i <= n; d[i, 0] = i++)
            {
            }

            for (int j = 0; j <= m; d[0, j] = j++)
            {
            }

            // Step 3
            for (int i = 1; i <= n; i++)
            {
                //Step 4
                for (int j = 1; j <= m; j++)
                {
                    // Step 5
                    int cost = (t[j - 1] == s[i - 1]) ? 0 : 1;

                    // Step 6
                    d[i, j] = Math.Min(
                        Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1),
                        d[i - 1, j - 1] + cost);
                    
                    //if(d[i,j] > threshold)
                    //{
                    //    return d[i, j];
                    //}
                }
            }
            // Step 7
            return d[n, m];
        }


        /// <summary>
        /// Check against a dictionary compiled from Facebook APIs.
        /// </summary>
        public Tuple<string, List<string>> CheckAgainstFacebookDict(string dictionary, string answer)
        {
            List<string> list = new JavaScriptSerializer().Deserialize<List<string>>(dictionary);

            HashSet<string> dict = new HashSet<string>(list.Select(x => x.ToLower()));

            Tuple<string, double, int> result = checkDictionary(dict, answer);
            List<string> distResult = checkDictionaryEditDistance(dict, answer);

            return new Tuple<string, List<string>>(result.Item1, distResult);
        }
    }
}