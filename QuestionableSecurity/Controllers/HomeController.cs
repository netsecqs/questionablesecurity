﻿using QuestionableSecurity.Business;
using QuestionableSecurity.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QuestionableSecurity.Controllers
{
    public class HomeController : Controller
    {
        private HomeBusiness homeBus;
        private string appDataPath;

        public HomeController()
        {
            homeBus = new HomeBusiness();
            appDataPath =  HttpRuntime.AppDomainAppPath + "App_Data\\";
        }

        public ActionResult Index()
        {
            ViewBag.Title = "Questionable Security";

            List<string> questions = homeBus.GetSecurityQuestions(HttpRuntime.Cache);
            SelectList questionList = homeBus.GetQuestionSelectList(questions);

            QuestionModel model = new QuestionModel()
            {
                Answer = "",
                QuestionId = 0,
                SecurityQuestions = questionList,
                Response = new ResponseModel()
                {
                    Answer = null,
                    CheckedServer = false,
                    CloseAnswers = new List<string>(),
                    EllapsedTime = 0.0
                }
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult Index(int QuestionId, string Answer)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            
            var result = homeBus.DictionaryAttack(QuestionId, Answer, HttpRuntime.Cache, appDataPath);

            
            List<string> questions = homeBus.GetSecurityQuestions(HttpRuntime.Cache);
            SelectList questionList = homeBus.GetQuestionSelectList(questions);

            stopwatch.Stop();

            QuestionModel model = new QuestionModel()
            {
                Answer = Answer,
                QuestionId = QuestionId,
                SecurityQuestions = questionList,
                Response = new ResponseModel()
                {
                    Answer = result.Item1.Item1,
                    CheckedServer = true,
                    EllapsedTime = stopwatch.ElapsedMilliseconds,
                    CloseAnswers = result.Item2
                }
            };

            return View(model);
        }

        public JsonResult GetListOfQuestions()
        {
            List<string> questions = homeBus.GetSecurityQuestions(HttpRuntime.Cache);

            return Json(questions, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckDictionary(string dictionary, string answer)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            Tuple<string, List<string>> result = homeBus.CheckAgainstFacebookDict(dictionary, answer);

            stopwatch.Stop();

            return Json(new {   
                                foundAnswer = result.Item1 != null, 
                                closeAnswers = result.Item2, 
                                elapsedTime = stopwatch.ElapsedMilliseconds
                            }
                , JsonRequestBehavior.AllowGet);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


    }
}