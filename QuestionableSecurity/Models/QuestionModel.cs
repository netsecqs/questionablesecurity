﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QuestionableSecurity.Models
{
    public class QuestionModel
    {
        public int QuestionId { get; set; }
        public string Answer { get; set; }
        public SelectList SecurityQuestions { get; set; }
        public ResponseModel Response { get; set; }
    }
}