﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuestionableSecurity.Models
{
    public class ResponseModel
    {
        public string Answer { get; set; }
        public double EllapsedTime { get; set; }
        public List<string> CloseAnswers { get; set; }
        public bool CheckedServer { get; set; }
    }
}